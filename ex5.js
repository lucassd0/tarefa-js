for(let i = 0; i < 5; i++){
    let num = prompt("Digite um número inteiro")
    if(num == undefined) break
    if(isNaN(num) || num != parseInt(num)){
        alert("Entrada inválida")
        i--
    }else{
        check(num)
    }
}

function check(num) {
    if(num % 3 == 0 && num % 5 == 0){
        console.log("fizzbuzz")
    }else if(num % 3 == 0){
        console.log("fizz")
    }else if(num % 5 == 0){
        console.log("buzz")
    }else return
}
