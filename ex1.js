var n1
var n2

do {
    n1 = prompt("Digite a dimensão da matriz 1 (ex: 2x2)").split("x")
    n2 = prompt("Digite a dimensão da matriz 2 (ex: 2x2)").split("x")

    if(n1[1] != n2[0]) {
        alert("Impossível realizar a operação")
    }else break

}while(true)

var m1
var m2

alert("Preenchendo a primeira matriz...\n")
m1 = preencheMatriz(n1[0], n1[1]);

alert("Preenchendo a segunda matriz...\n")
m2 = preencheMatriz(n2[0], n2[1]);

alert("Matriz 1:\n" + m1.join("\n") + "\nMatriz 2:\n" + m2.join("\n") + "\nResultante:\n" + multiplicaMatriz(m1, m2).join("\n"))

function preencheMatriz(n, c) {
    let matrix = []
    for(let i = 0; i < n; i++){
        let line = prompt("Digite os "+ c +" elementos da linha " + (i+1) + " da matriz (ex: 2 4)").split(" ")
        if(line.length != c){
            alert("Entrada inválida, por favor tente novamente")
            i--;
        }else matrix.push(line)
    }

    return matrix;
}

function multiplicaMatriz(m1, m2) {
    let n = m1.length
    let c = m2[0].length

    let re_matrix = []
    
    for(let i = 0; i < n; i++){
        let line = []
        for(let j = 0; j < c; j++){
            let aux = 0
            for(let k = 0; k < m2.length; k++){
                aux += m1[i][k] * m2[k][j]
            }
            line.push(aux)
        }
        re_matrix.push(line)
    }

    return re_matrix
}
