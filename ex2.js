var temp
do {
    temp = prompt("Digite uma temperatura em ºF (apenas números)")
    if(temp == "" || isNaN(temp)) alert("Entrada inválida")
}while(temp == "" || isNaN(temp))


temp == undefined ? alert("Operação cancelada, recarregue a página para tentar novamente"): alert(temp + "ºF são equivalentes a " + calcTempCelsius(temp) +"ºC")

function calcTempCelsius(temp) {
    return (temp - 32)/1.8
}
