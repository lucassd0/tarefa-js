//Q1
for (god of gods) console.log(god.name + " " + god.features.length)

//Q2
gods.forEach((god) => {
    if (god.roles.includes("Mid")) console.log(god)
})

//Q3
gods.sort((god_a, god_b) => {
    if(god_a.pantheon < god_b.pantheon){
        return -1
    }
    if(god_a.pantheon > god_b.pantheon){
        return 1
    }

    return 0
})

console.log(gods)

//Q4
var re_gods = gods.map(god => {
    let aux = []
    aux = god.name + " (" + god.class + ")"
    return aux

})

console.log(re_gods)
